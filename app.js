var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mysql = require('mysql');
var methodOverride=require('method-override');


var users = require('./routes/usuario');
var cats = require('./routes/categoria');
var emps = require('./routes/empresa');
var proms = require('./routes/promocion');
var sucs = require('./routes/sucursal');
var coms = require('./routes/comentario');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/usuario', users);
app.use('/categoria', cats);
app.use('/empresa', emps);
app.use('/promocion', proms);
app.use('/sucursal', sucs);
app.use('/comentario',coms);

// catch 404 and forward t  o error handler
app.use(function(req, res, next) {
  var err = new Error('Página no encontrada');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.jsonp({error:err.message});
});

module.exports = app;
