var cnx = require('../controllers/cnx');
function Empresa() {
    Empresa.id_empresa=0;
    Empresa.nombre="";
}
Empresa.prototype.set=function (id, nom) {
    Empresa.id_empresa=id;
    Empresa.nombre=nom;
}
Empresa.prototype.getAll=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('SELECT * from empresa',function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve(rows);
            });
            connection.release();
        });
    });
    return promise;
};
Empresa.prototype.save=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('INSERT INTO empresa (nombre) VALUES (?)',[Empresa.nombre],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({id_Empresa:rows.insertId});
            });
            connection.release();
        });
    });
    return promise;
};
Empresa.prototype.getOne=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('SELECT * FROM empresa where id_empresa = ?',[Empresa.id_empresa],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve(rows[0]);
            });
            connection.release();
        });
    });
    return promise;
}
Empresa.prototype.update=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('UPDATE empresa set nombre = ? where id_empresa = ?',[Empresa.nombre, Empresa.id_empresa],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({message:'ok'});
            });
            connection.release();
        });
    });
    return promise;
}
Empresa.prototype.delete=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('delete from empresa where id_empresa = ? ',[Empresa.id_empresa],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({message:'ok'});
            });
            connection.release();
        });
    });
    return promise;
}
Empresa.prototype.forPatch=function (data) {
    console.log(data);
    var snt="";
    var val='';
    if (data.nombre)
    {
        snt='nombre = ?';
        val=data.nombre;
    }
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('update empresa set '+snt+' where id_empresa = ? ',[val, Empresa.id_empresa],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({message:'ok'});
            });
            connection.release();
        });
    });
    return promise;
}
Empresa.prototype.sucursales=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('SELECT * from sucursal where id_empresa = ?', [Empresa.id_empresa],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve(rows);
            });
            connection.release();
        });
    });
    return promise;
}
module.exports = Empresa;
