//- `200` **Ok**. La petición del navegador se ha completado con éxito.
//- `204` **No Content**. La petición se ha completado con éxito pero su respuesta no tiene ningún contenido.
//- `400` **Bad Request**. El servidor no es capaz de entender la petición del navegador porque su sintaxis no es correcta.
//- `401` **Unauthorized**. El recurso solicitado por el navegador requiere de autenticación.
//- `404` **Not Found**. El servidor no puede encontrar el recurso solicitado por el navegador.
//- `405` **Method Not Allowed**. El navegador ha utilizado un método (GET, POST, etc.) no permitido por el servidor para obtener ese recurso.
//- `412` **Precondition Failed**. El servidor no es capaz de cumplir con algunas de las condiciones impuestas por el navegador en su petición.
//- `422` **Unprocessable Entity**. La petición del navegador tiene el formato correcto, pero sus contenidos tienen algún error semántico que impide al servidor responder.
//- `500` **Internal Server Error**. Se ha producido un error interno.
function Mensaje(code,data){
    this.code=code;
    if(code==200)
        this.message=data;
    if (code==404)
        this.message='El servidor no puede encontrar el recurso';
    if (code==422)
        this.message='Faltan datos';
}
Mensaje.prototype.responder=function () {

}
