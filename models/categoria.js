var cnx = require('../controllers/cnx');
function Categoria() {
    Categoria.id_categoria=0;
    Categoria.nombre="";
}
Categoria.prototype.set=function (id, nom) {
    Categoria.id_categoria=id;
    Categoria.nombre=nom;
}
Categoria.prototype.getAll=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('SELECT * from categoria',function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve(rows);
            });
            connection.release();
        });
    });
    return promise;
};
Categoria.prototype.save=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('INSERT INTO categoria (nombre) VALUES (?)',[Categoria.nombre],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({id_Categoria:rows.insertId});
            });
            connection.release();
        });
    });
    return promise;
};
Categoria.prototype.getOne=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('SELECT * FROM categoria where id_categoria = ?',[Categoria.id_categoria],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve(rows[0]);
            });
            connection.release();
        });
    });
    return promise;
}
Categoria.prototype.update=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('UPDATE Categoria set nombre = ? where id_categoria = ?',[Categoria.nombre, Categoria.id_categoria],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({message:'ok'});
            });
            connection.release();
        });
    });
    return promise;
}
Categoria.prototype.delete=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('delete from categoria where id_categoria = ? ',[Categoria.id_categoria],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({message:'ok'});
            });
            connection.release();
        });
    });
    return promise;
}
Categoria.prototype.forPatch=function (data) {
    console.log(data);
    var snt="";
    var val='';
    if (data.nombre)
    {
        snt='nombre = ?';
        val=data.nombre;
    }
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('update categoria set '+snt+' where id_categoria = ? ',[val, Categoria.id_categoria],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({message:'ok'});
            });
            connection.release();
        });
    });
    return promise;
}
module.exports = Categoria;
