var cnx = require('../controllers/cnx');
function Usuario() {
    Usuario.id_usuario=0;
    Usuario.email="";
    Usuario.password="";
    Usuario.nombre_usuario="";
    Usuario.rol="";
}
Usuario.prototype.set=function (id, email, pass, nom, rol) {
    Usuario.id_usuario=id;
    Usuario.email=email;
    Usuario.password=pass;
    Usuario.nombre_usuario=nom;
    Usuario.rol=rol;
}
Usuario.prototype.getAll=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('SELECT * from usuario',function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve(rows);
            });
            connection.release();
        });
    });
    return promise;
};
Usuario.prototype.save=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('INSERT INTO usuario (email, password, nombre_usuario, rol) VALUES (?,?,?,?)',[Usuario.email, Usuario.password, Usuario.nombre_usuario, Usuario.rol],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({id_usuario:rows.insertId});
            });
            connection.release();
        });
    });
    return promise;
};
Usuario.prototype.getOne=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('SELECT * FROM usuario where id_usuario = ?',[Usuario.id_usuario],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve(rows[0]);
            });
            connection.release();
        });
    });
    return promise;
};
Usuario.prototype.update=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('UPDATE usuario set email = ?, password = ?, nombre_usuario = ?, rol = ? where id_usuario = ?',[Usuario.email, Usuario.password, Usuario.nombre_usuario, Usuario.rol, Usuario.id_usuario],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({message:'ok'});
            });
            connection.release();
        });
    });
    return promise;
};
Usuario.prototype.delete=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('delete from usuario where id_usuario = ? ',[Usuario.id_usuario],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({message:'ok'});
            });
            connection.release();
        });
    });
    return promise;
};
Usuario.prototype.forPatch=function (data) {
    console.log(data);
    var snt="";
    var val='';
    if(data.email)
    {
        snt='email = ?';
        val=data.email;
    }
    else if (data.password)
    {
        snt='password = ?';
        val=data.password;
    }
    else if (data.nombre_usuario)
    {
        snt='nombre_usuario = ?';
        val=data.nombre_usuario;
    }
    else if (data.rol)
    {
        snt='rol = ?';
        val=data.rol;
    }
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('update usuario set '+snt+' where id_usuario = ? ',[val, Usuario.id_usuario],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({message:'ok'});
            });
            connection.release();
        });
    });
    return promise;
};
Usuario.prototype.login=function () {
    console.log("ok-");
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('SELECT * from usuario where email = ? and password = ?',[Usuario.email, Usuario.password],function (err,rows) {
                if(err)
                {
                    console.warn(err);
                    reject(err);
                }
                else
                    resolve(rows);
            });
            connection.release();
        });
    });
    return promise;
};
module.exports = Usuario;