var cnx = require('../controllers/cnx');
function Comentario() {
    Comentario.id_comentario=0;
    Comentario.detalle="";
    Comentario.puntuacion=0;
    Comentario.id_usuario=0;
    Comentario.id_promocion_sucursal=0;
}
Comentario.prototype.set=function (id, det, pun, idu, idps) {
    Comentario.id_comentario=id;
    Comentario.detalle=det;
    Comentario.puntuacion=pun;
    Comentario.id_usuario=idu;
    Comentario.id_promocion_sucursal=idps;
}
Comentario.prototype.getAll=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('SELECT * from comentario',function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve(rows);
            });
            connection.release();
        });
    });
    return promise;
};
Comentario.prototype.save=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('INSERT INTO comentario (detalle, puntuacion, id_usuario, id_promocion_sucursal) VALUES (?,?,?,?)',[Comentario.detalle, Comentario.puntuacion, Comentario.id_usuario, Comentario.id_promocion_sucursal],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({id_comentario:rows.insertId});
            });
            connection.release();
        });
    });
    return promise;
};
Comentario.prototype.getOne=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('SELECT * FROM comentario where id_comentario = ?',[Comentario.id_comentario],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve(rows[0]);
            });
            connection.release();
        });
    });
    return promise;
};
Comentario.prototype.update=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('UPDATE comentario set detalle = ?, puntuacion = ?, id_usuario = ?, id_promocion_sucursal = ? where id_comentario = ?',[Comentario.detalle, Comentario.puntuacion, Comentario.id_usuario, Comentario.id_promocion_sucursal, Comentario.id_empresa, Comentario.id_comentario],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({message:'ok'});
            });
            connection.release();
        });
    });
    return promise;
};
Comentario.prototype.delete=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('delete from comentario where id_comentario = ? ',[Comentario.id_comentario],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({message:'ok'});
            });
            connection.release();
        });
    });
    return promise;
};
Comentario.prototype.forPatch=function (data) {
    console.log(data);
    var snt="";
    var val='';
    if(data.detalle)
    {
        snt='detalle = ?';
        val=data.detalle;
    }
    else if (data.puntuacion)
    {
        snt='puntuacion = ?';
        val=data.puntuacion;
    }
    else if (data.id_usuario)
    {
        snt='id_usuario = ?';
        val=data.id_usuario;
    }
    else if (data.id_promocion_sucursal)
    {
        snt='id_promocion_sucursal = ?';
        val=data.id_promocion_sucursal;
    }
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('update comentario set '+snt+' where id_comentario = ? ',[val, Comentario.id_comentario],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({message:'ok'});
            });
            connection.release();
        });
    });
    return promise;
};
Comentario.prototype.getxPromocion=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('SELECT * from comentario c, usuario u, promocion_sucursal ps, sucursal s WHERE c.id_usuario=u.id_usuario and c.id_promocion_sucursal=ps.id_promocion_sucursal and s.id_sucursal=ps.id_sucursal and ps.id_promocion = ? ',[Comentario.id_promocion_sucursal],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve(rows);
            });
            connection.release();
        });
    });
    return promise;
};
module.exports = Comentario;