var cnx = require('../controllers/cnx');
function Promocion() {
    Promocion.id_promocion=0;
    Promocion.nombre="";
    Promocion.descripcion="";
    Promocion.url_imagen="";
    Promocion.id_categoria=0;
    Promocion.id_empresa=0;
}
Promocion.prototype.set=function (id, nom, des, url, idc, ide) {
    Promocion.id_promocion=id;
    Promocion.nombre=nom;
    Promocion.descripcion=des;
    Promocion.url_imagen=url;
    Promocion.id_categoria=idc;
    Promocion.id_empresa=ide;
}
Promocion.prototype.getAll=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('SELECT * from promocion',function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve(rows);
            });
            connection.release();
        });
    });
    return promise;
};
Promocion.prototype.save=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('INSERT INTO promocion (nombre, descripcion, url_imagen, id_categoria, id_empresa) VALUES (?,?,?,?,?)',[Promocion.nombre, Promocion.descripcion, Promocion.url_imagen, Promocion.id_categoria, Promocion.id_empresa],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({id_promocion:rows.insertId});
            });
            connection.release();
        });
    });
    return promise;
};
Promocion.prototype.getOne=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('SELECT * FROM promocion where id_promocion = ?',[Promocion.id_promocion],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve(rows[0]);
            });
            connection.release();
        });
    });
    return promise;
};
Promocion.prototype.update=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('UPDATE promocion set nombre = ?, descripcion = ?, url_imagen = ?, id_categoria = ?, id_empresa = ? where id_promocion = ?',[Promocion.nombre, Promocion.descripcion, Promocion.url_imagen, Promocion.id_categoria, Promocion.id_empresa, Promocion.id_promocion],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({message:'ok'});
            });
            connection.release();
        });
    });
    return promise;
};
Promocion.prototype.delete=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('delete from promocion where id_promocion = ? ',[Promocion.id_promocion],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({message:'ok'});
            });
            connection.release();
        });
    });
    return promise;
};
Promocion.prototype.forPatch=function (data) {
    console.log(data);
    var snt="";
    var val='';
    if(data.nombre)
    {
        snt='nombre = ?';
        val=data.nombre;
    }
    else if (data.descripcion)
    {
        snt='descripcion = ?';
        val=data.descripcion;
    }
    else if (data.url_imagen)
    {
        snt='url_imagen = ?';
        val=data.url_imagen;
    }
    else if (data.id_categoria)
    {
        snt='id_categoria = ?';
        val=data.id_categoria;
    }
    else if (data.id_empresa){
        snt='id_empresa = ?';
        val=data.id_empresa;
    }
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('update promocion set '+snt+' where id_promocion = ? ',[val, Promocion.id_promocion],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({message:'ok'});
            });
            connection.release();
        });
    });
    return promise;
};
Promocion.prototype.addSucursal=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('INSERT INTO promocion_sucursal (fecha_limite, id_promocion, id_sucursal) VALUES (?,?,?)',[Promocion.id_empresa, Promocion.id_promocion, Promocion.id_categoria],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({id_promocion_sucursal:rows.insertId});
            });
            connection.release();
        });
    });
    return promise;
};
Promocion.prototype.getxLatLon=function (data) {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('SELECT *, s.nombre as nombre_sucursal, e.nombre as nombre_empresa, c.nombre as nombre_catgoria, p.nombre as nombre_promocion FROM sucursal s, promocion p, promocion_sucursal ps, empresa e, categoria c WHERE ps.id_promocion=p.id_promocion AND ps.id_sucursal=s.id_sucursal AND p.id_empresa=e.id_empresa AND p.id_categoria=c.id_categoria ',function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve(rows);
            });
            connection.release();
        });
    });
    return promise;
};
module.exports = Promocion;