var cnx = require('../controllers/cnx');
function Sucursal() {
    Sucursal.id_sucursal=0;
    Sucursal.nombre="";
    Sucursal.direccion="";
    Sucursal.latitud=0;
    Sucursal.longitud=0;
    Sucursal.id_empresa=0;
}
Sucursal.prototype.set=function (id, nom, dir, lat, lon, ide) {
    Sucursal.id_sucursal=id;
    Sucursal.nombre=nom;
    Sucursal.direccion=dir;
    Sucursal.latitud=lat;
    Sucursal.longitud=lon;
    Sucursal.id_empresa=ide;
}
Sucursal.prototype.getAll=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('SELECT * from sucursal',function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve(rows);
            });
            connection.release();
        });
    });
    return promise;
};
Sucursal.prototype.save=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('INSERT INTO sucursal (nombre, direccion, latitud, longitud, id_empresa) VALUES (?,?,?,?,?)',[Sucursal.nombre, Sucursal.direccion, Sucursal.latitud, Sucursal.longitud, Sucursal.id_empresa],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({id_sucursal:rows.insertId});
            });
            connection.release();
        });
    });
    return promise;
};
Sucursal.prototype.getOne=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('SELECT * FROM sucursal where id_sucursal = ?',[Sucursal.id_sucursal],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve(rows[0]);
            });
            connection.release();
        });
    });
    return promise;
}
Sucursal.prototype.update=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('UPDATE sucursal set nombre = ?, direccion = ?, latitud = ?, longitud = ?, id_empresa = ? where id_sucursal = ?',[Sucursal.nombre, Sucursal.direccion, Sucursal.latitud, Sucursal.longitud, Sucursal.id_empresa, Sucursal.id_sucursal],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({message:'ok'});
            });
            connection.release();
        });
    });
    return promise;
}
Sucursal.prototype.delete=function () {
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('delete from sucursal where id_sucursal = ? ',[Sucursal.id_sucursal],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({message:'ok'});
            });
            connection.release();
        });
    });
    return promise;
}
Sucursal.prototype.forPatch=function (data) {
    console.log(data);
    var snt="";
    var val='';
    if(data.nombre)
    {
        snt='nombre = ?';
        val=data.nombre;
    }
    else if (data.direccion)
    {
        snt='direccion = ?';
        val=data.direccion;
    }
    else if (data.latitud)
    {
        snt='latitud = ?';
        val=data.latitud;
    }
    else if (data.longitud)
    {
        snt='longitud = ?';
        val=data.longitud;
    }
    else if (data.id_empresa){
        snt='id_empresa = ?';
        val=data.id_empresa;
    }
    var promise =new Promise(function (resolve, reject) {
        cnx.getConnection(function (err, connection) {
            connection.query('update sucursal set '+snt+' where id_sucursal = ? ',[val, Sucursal.id_sucursal],function (err,rows) {
                if(err)
                    reject(err);
                else
                    resolve({message:'ok'});
            });
            connection.release();
        });
    });
    return promise;
}
module.exports = Sucursal;