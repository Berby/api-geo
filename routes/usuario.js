var express = require('express');
var router = express.Router();
var Usuario = require('../models/usuario');
Usuario=new Usuario();
router.route('/')
    .get(function(req, res) {
        var promise = Usuario.getAll();
        promise.then(function (data) {
            console.log(data);
            res.jsonp(data);
        }, function (err) {
            console.warn(err);
            res.jsonp(err);
        });
    })
    .post(function (req, res) {
        if(req.body.email && req.body.password && req.body.nombre_usuario){
            Usuario.set(0, req.body.email, req.body.password, req.body.nombre_usuario, 'usuario');
            var promise = Usuario.save();
            promise.then(function (data) {
                res.jsonp(data);
            }, function (err) {
                res.jsonp(err);
            });
        }
        else{
            res.jsonp({code:422,message:"No Colocaste todos los datos"});
        }
    });
router.route('/:id_usuario')
    .get(function (req, res) {
        if(req.params.id_usuario){
            Usuario.set(req.params.id_usuario,'','','','');
            var promise = Usuario.getOne();
            promise.then(function (data) {
                console.log(data);
                res.jsonp(data);
            }, function (err) {
                console.warn(err);
                res.jsonp(err);
            });
        }
        else{
            res.jsonp({code:422,message:"No Colocaste todos los datos"});
        }
    })
    .delete(function (req, res) {
        if(req.params.id_usuario){
            Usuario.set(req.params.id_usuario,'','','','');
            var promise = Usuario.delete();
            promise.then(function (data) {
                console.log(data);
                res.jsonp(data);
            }, function (err) {
                console.warn(err);
                res.jsonp(err);
            });
        }
        else{
            res.jsonp({code:422,message:"No Colocaste todos los datos"});
        }
    })
    .put(function (req, res) {
        if(req.params.id_usuario && req.body.email && req.body.password && req.body.nombre_usuario && req.body.rol){
            Usuario.set(req.params.id_usuario,req.body.email,req.body.password,req.body.nombre_usuario,req.body.rol);
            var promise = Usuario.update();
            promise.then(function (data) {
                console.log(data);
                res.jsonp(data);
            }, function (err) {
                console.warn(err);
                res.jsonp(err);
            });
        }
        else{
            res.jsonp({code:422,message:"No Colocaste todos los datos"});
        }
    })
    .patch(function (req, res) {
        if(req.params.id_usuario){
            Usuario.set(req.params.id_usuario,'','','','');
            var promise = Usuario.forPatch(req.body);
            promise.then(function (data) {
                console.log(data);
                res.jsonp(data);
            }, function (err) {
                console.warn(err);
                res.jsonp(err);
            });
        }
        else{
            res.jsonp({code:422,message:"No Colocaste todos los datos"});
        }
    });
router.route('/login/')
    .post(function (req, res) {
        if(req.body.email && req.body.password){
            Usuario.set(0,req.body.email,req.body.password,'','');
            var promise = Usuario.login();
            promise.then(function (data) {
                data.url='/Usuario/';
                console.log(data);
                res.jsonp(data);
            }, function (err) {
                res.jsonp(err);
            });
        } else {
            res.jsonp({code:422, message:'Introduzca el email y la contraseña'});
        }
    });
module.exports = router;
