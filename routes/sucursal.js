var express = require('express');
var router = express.Router();
var Sucursal = require('../models/sucursal');
Sucursal=new Sucursal();
router.route('/')
    .get(function(req, res) {
        var promise = Sucursal.getAll();
        promise.then(function (data) {
            console.log(data);
            res.jsonp(data);
        }, function (err) {
            console.warn(err);
            res.jsonp(err);
        });
    })
    .post(function (req, res) {
        if(req.body.nombre && req.body.direccion && req.body.latitud && req.body.longitud && req.body.id_empresa){
            Sucursal.set(0, req.body.nombre, req.body.direccion, req.body.latitud, req.body.longitud, req.body.id_empresa);
            var promise = Sucursal.save();
            promise.then(function (data) {
                res.jsonp(data);
            }, function (err) {
                res.jsonp(err);
            });
        }
        else{
            res.jsonp({code:422,message:"No Colocaste todos los datos"});
        }
    });
router.route('/:id_sucursal')
    .get(function (req, res) {
        Sucursal.set(req.params.id_sucursal,'','',0,0,0);
        var promise = Sucursal.getOne();
        promise.then(function (data) {
            console.log(data);
            res.jsonp(data);
        }, function (err) {
            console.warn(err);
            res.jsonp(err);
        });
    })
    .delete(function (req, res) {
        Sucursal.set(req.params.id_sucursal,'','',0,0,0);
        var promise = Sucursal.delete();
        promise.then(function (data) {
            console.log(data);
            res.jsonp(data);
        }, function (err) {
            console.warn(err);
            res.jsonp(err);
        });
    })
    .put(function (req, res) {
        if(req.body.nombre && req.body.direccion && req.body.latitud && req.body.longitud && req.body.id_empresa){
            Sucursal.set(req.params.id_sucursal, req.body.nombre, req.body.direccion, req.body.latitud, req.body.longitud, req.body.id_empresa);
            var promise = Sucursal.update();
            promise.then(function (data) {
                console.log(data);
                res.jsonp(data);
            }, function (err) {
                console.warn(err);
                res.jsonp(err);
            });
        }
        else{
            res.jsonp({code:422,message:"No Colocaste todos los datos"});
        }
    })
    .patch(function (req, res) {
        Sucursal.set(req.params.id_sucursal,'','','',0,0,0);
        var promise = Sucursal.forPatch(req.body);
        promise.then(function (data) {
            console.log(data);
            res.jsonp(data);
        }, function (err) {
            console.warn(err);
            res.jsonp(err);
        });
    });
module.exports = router;
