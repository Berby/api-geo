var express = require('express');
var router = express.Router();
var Empresa = require('../models/empresa');
Empresa=new Empresa();
router.route('/')
    .get(function(req, res) {
        var promise = Empresa.getAll();
        promise.then(function (data) {
            console.log(data);
            res.jsonp(data);
        }, function (err) {
            console.warn(err);
            res.jsonp(err);
        });
    })
    .post(function (req, res) {
        if(req.body.nombre){
            Empresa.set(0, req.body.nombre);
            console.log(Empresa);
            var promise = Empresa.save();
            promise.then(function (data) {
                res.jsonp(data);
            }, function (err) {
                res.jsonp(err);
            });
        }
        else{
            res.jsonp({code:422,message:"No Colocaste todos los datos"});
        }
    });
router.route('/:id_empresa')
    .get(function (req, res) {
        Empresa.set(req.params.id_empresa,'','','','');
        var promise = Empresa.getOne();
        promise.then(function (data) {
            console.log(data);
            res.jsonp(data);
        }, function (err) {
            console.warn(err);
            res.jsonp(err);
        });
    })
    .delete(function (req, res) {
        Empresa.set(req.params.id_empresa,'','','','');
        var promise = Empresa.delete();
        promise.then(function (data) {
            console.log(data);
            res.jsonp(data);
        }, function (err) {
            console.warn(err);
            res.jsonp(err);
        });
    })
    .put(function (req, res) {
        if(req.body.nombre){
            Empresa.set(req.params.id_empresa,req.body.nombre);
            var promise = Empresa.update();
            promise.then(function (data) {
                console.log(data);
                res.jsonp(data);
            }, function (err) {
                console.warn(err);
                res.jsonp(err);
            });
        }
        else{
            res.jsonp({code:422,message:"No Colocaste todos los datos"});
        }
    })
    .patch(function (req, res) {
        Empresa.set(req.params.id_empresa,'','','','');
        var promise = Empresa.forPatch(req.body);
        promise.then(function (data) {
            console.log(data);
            res.jsonp(data);
        }, function (err) {
            console.warn(err);
            res.jsonp(err);
        });
    });
router.route('/:id_empresa/sucursales')
    .get(function (req, res) {
        Empresa.set(req.params.id_empresa,'','','','');
        var promise = Empresa.sucursales();
        promise.then(function (data) {
            console.log(data);
            res.jsonp(data);
        }, function (err) {
            console.warn(err);
            res.jsonp(err);
        });
    });
module.exports = router;