var express = require('express');
var router = express.Router();
var Promocion = require('../models/promocion');
Promocion=new Promocion();
router.route('/')
    .get(function(req, res) {
        var promise = Promocion.getAll();
        promise.then(function (data) {
            console.log(data);
            res.jsonp(data);
        }, function (err) {
            console.warn(err);
            res.jsonp(err);
        });
    })
    .post(function (req, res) {
        if(req.body.nombre && req.body.descripcion && req.body.url_imagen && req.body.id_categoria && req.body.id_empresa){
            Promocion.set(0, req.body.nombre,req.body.descripcion,req.body.url_imagen,req.body.id_categoria,req.body.id_empresa);
            console.log(Promocion);
            var promise = Promocion.save();
            promise.then(function (data) {
                res.jsonp(data);
            }, function (err) {
                res.jsonp(err);
            });
        }
        else{
            res.jsonp({code:422,message:"No Colocaste todos los datos"});
        }
    });
router.route('/:id_promocion')
    .get(function (req, res) {
        Promocion.set(req.params.id_promocion,'','','',0,0);
        var promise = Promocion.getOne();
        promise.then(function (data) {
            console.log(data);
            res.jsonp(data);
        }, function (err) {
            console.warn(err);
            res.jsonp(err);
        });
    })
    .delete(function (req, res) {
            Promocion.set(req.params.id_promocion,'','','',0,0);
            var promise = Promocion.delete();
            promise.then(function (data) {
                console.log(data);
                res.jsonp(data);
            }, function (err) {
                console.warn(err);
                res.jsonp(err);
            });
    })
    .put(function (req, res) {
        if(req.body.nombre && req.body.descripcion && req.body.url_imagen && req.body.id_categoria && req.body.id_empresa){
            Promocion.set(req.params.id_promocion,req.body.nombre,req.body.descripcion,req.body.url_imagen,req.body.id_categoria,req.body.id_empresa);
            var promise = Promocion.update();
            promise.then(function (data) {
                console.log(data);
                res.jsonp(data);
            }, function (err) {
                console.warn(err);
                res.jsonp(err);
            });
        }
        else{
            res.jsonp({code:422,message:"No Colocaste todos los datos"});
        }
    })
    .patch(function (req, res) {
        Promocion.set(req.params.id_promocion,'','','',0,0);
        var promise = Promocion.forPatch(req.body);
        promise.then(function (data) {
            console.log(data);
            res.jsonp(data);
        }, function (err) {
            console.warn(err);
            res.jsonp(err);
        });
    });
router.route('/sucursal')
    .post(function (req, res) {
        if(req.body.id_promocion && req.body.id_sucursal && req.body.fecha_limite){
            Promocion.set(req.body.id_promocion, '','','',req.body.id_sucursal,req.body.fecha_limite);
            var promise = Promocion.addSucursal();
            promise.then(function (data) {
                res.jsonp(data);
            }, function (err) {
                res.jsonp(err);
            });
        }
        else{
            res.jsonp({code:422,message:"No Colocaste todos los datos"});
        }
    });
router.route('/busca')
    .post(function (req, res) {
        if(req.body.latitud && req.body.longitud){
            var promise = Promocion.getxLatLon(req.body);
            promise.then(function (data) {
                res.jsonp(data);
            }, function (err) {
                res.jsonp(err);
            });
        }
        else{
            res.status(422);
            res.jsonp({code:422,message:"No Colocaste todos los datos"});
        }
    });
module.exports = router;
