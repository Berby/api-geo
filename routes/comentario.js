var express = require('express');
var router = express.Router();
var Comentario = require('../models/comentario');
Comentario=new Comentario();
router.route('/')
    .get(function(req, res) {
        var promise = Comentario.getAll();
        promise.then(function (data) {
            console.log(data);
            res.jsonp(data);
        }, function (err) {
            console.warn(err);
            res.jsonp(err);
        });
    })
    .post(function (req, res) {
        if(req.body.detalle && req.body.puntuacion && req.body.id_usuario && req.body.id_promocion_sucursal){
            Comentario.set(0, req.body.detalle,req.body.puntuacion,req.body.id_usuario,req.body.id_promocion_sucursal);
            var promise = Comentario.save();
            promise.then(function (data) {
                res.jsonp(data);
            }, function (err) {
                res.jsonp(err);
            });
        }
        else{
            res.jsonp({code:422,message:"No Colocaste todos los datos"});
        }
    });
router.route('/:id_comentario')
    .get(function (req, res) {
        Comentario.set(req.params.id_comentario,'',0,0,0);
        var promise = Comentario.getOne();
        promise.then(function (data) {
            console.log(data);
            res.jsonp(data);
        }, function (err) {
            console.warn(err);
            res.jsonp(err);
        });
    })
    .delete(function (req, res) {
        Comentario.set(req.params.id_comentario,'',0,0,0);
        var promise = Comentario.delete();
        promise.then(function (data) {
            console.log(data);
            res.jsonp(data);
        }, function (err) {
            console.warn(err);
            res.jsonp(err);
        });
    })
    .put(function (req, res) {
        if(req.body.detalle && req.body.puntuacion && req.body.id_usuario && req.body.id_promocion_sucursal){
            Comentario.set(req.params.id_comentario,req.body.detalle,req.body.puntuacion,req.body.id_usuario,req.body.id_promocion_sucursal);
            var promise = Comentario.update();
            promise.then(function (data) {
                console.log(data);
                res.jsonp(data);
            }, function (err) {
                console.warn(err);
                res.jsonp(err);
            });
        }
        else{
            res.jsonp({code:422,message:"No Colocaste todos los datos"});
        }
    })
    .patch(function (req, res) {
        Comentario.set(req.params.id_comentario,'',0,0,0);
        var promise = Comentario.forPatch(req.body);
        promise.then(function (data) {
            console.log(data);
            res.jsonp(data);
        }, function (err) {
            console.warn(err);
            res.jsonp(err);
        });
    });
router.route('/promocion/:id_promocion')
    .get(function (req, res) {
        Comentario.set(0,'',0,0,req.params.id_promocion);
        var promise = Comentario.getxPromocion();
        promise.then(function (data) {
            console.log(data);
            res.jsonp(data);
        }, function (err) {
            console.warn(err);
            res.jsonp(err);
        });
    });
module.exports = router;
