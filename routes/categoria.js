var express = require('express');
var router = express.Router();
var Categoria = require('../models/categoria');
Categoria=new Categoria();
router.route('/')
    .get(function(req, res) {
        var promise = Categoria.getAll();
        promise.then(function (data) {
            console.log(data);
            res.jsonp(data);
        }, function (err) {
            console.warn(err);
            res.jsonp(err);
        });
    })
    .post(function (req, res) {
        if(req.body.nombre){
            Categoria.set(0, req.body.nombre);
            console.log(Categoria);
            var promise = Categoria.save();
            promise.then(function (data) {
                res.jsonp(data);
            }, function (err) {
                res.jsonp(err);
            });
        }
        else{
            res.jsonp({code:422,message:"No Colocaste todos los datos"});
        }
    });
router.route('/:id_categoria')
    .get(function (req, res) {
            Categoria.set(req.params.id_categoria,'','','','');
            var promise = Categoria.getOne();
            promise.then(function (data) {
                console.log(data);
                res.jsonp(data);
            }, function (err) {
                console.warn(err);
                res.jsonp(err);
            });
    })
    .delete(function (req, res) {
            Categoria.set(req.params.id_categoria,'','','','');
            var promise = Categoria.delete();
            promise.then(function (data) {
                console.log(data);
                res.jsonp(data);
            }, function (err) {
                console.warn(err);
                res.jsonp(err);
            });
    })
    .put(function (req, res) {
        if(req.body.nombre){
            Categoria.set(req.params.id_categoria,req.body.nombre);
            var promise = Categoria.update();
            promise.then(function (data) {
                console.log(data);
                res.jsonp(data);
            }, function (err) {
                console.warn(err);
                res.jsonp(err);
            });
        }
        else{
            res.jsonp({code:422,message:"No Colocaste todos los datos"});
        }
    })
    .patch(function (req, res) {
        Categoria.set(req.params.id_Categoria,'','','','');
        var promise = Categoria.forPatch(req.body);
        promise.then(function (data) {
            console.log(data);
            res.jsonp(data);
        }, function (err) {
            console.warn(err);
            res.jsonp(err);
        });
    });
module.exports = router;

