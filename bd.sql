CREATE DATABASE geo DEFAULT CHARACTER SET UTF8 COLLATE utf8_bin;
USE geo;

CREATE TABLE usuario (
	id_usuario int NOT NULL AUTO_INCREMENT,
	email varchar(200),
	password varchar(200),
	nombre_usuario varchar(200),
	rol varchar(20), -- Usuario, Publicista, Administrador
	_createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	_updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id_usuario)
);
CREATE TABLE empresa (
    id_empresa int NOT NULL AUTO_INCREMENT,
    nombre varchar(100),
    _createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    _updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id_empresa)
);
CREATE TABLE publicista_empresa (
    id_publicista_empresa int NOT NULL AUTO_INCREMENT,
    id_empresa int NOT NULL,
    id_usuario int NOT NULL,
    _createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    _updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id_publicista_empresa),
    FOREIGN KEY (id_empresa) REFERENCES empresa (id_empresa),
    FOREIGN KEY (id_usuario) REFERENCES usuario (id_usuario)
);
CREATE TABLE sucursal (
    id_sucursal int NOT NULL AUTO_INCREMENT,
    nombre varchar(100),
    direccion varchar(255),
    latitud float,
    longitud float,
    id_empresa int NOT NULL,
    _createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    _updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id_sucursal),
    FOREIGN KEY (id_empresa) REFERENCES empresa (id_empresa)
);
CREATE TABLE categoria (
    id_categoria int NOT NULL AUTO_INCREMENT,
    nombre varchar(50),
    _createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    _updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id_categoria)
);
CREATE TABLE promocion (
    id_promocion int NOT NULL AUTO_INCREMENT,
    nombre varchar(100),
    descripcion text,
    url_imagen varchar(200),
    id_categoria int NOT NULL,
    id_empresa int NOT NULL,
    _createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    _updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id_promocion),
    FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria),
    FOREIGN KEY (id_empresa) REFERENCES empresa (id_empresa)
);
CREATE TABLE promocion_sucursal (
    id_promocion_sucursal int NOT NULL AUTO_INCREMENT,
    fecha_limite datetime,
    id_promocion int NOT NULL,
    id_sucursal int NOT NULL,
    _createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    _updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id_promocion_sucursal),
    FOREIGN KEY (id_promocion) REFERENCES promocion(id_promocion),
    FOREIGN KEY (id_sucursal) REFERENCES sucursal (id_sucursal)
);
CREATE TABLE filtro_categoria (
    id_usuario int NOT NULL,
    id_categoria int NOT NULL,
    _createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    _updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario),
    FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria)
);
CREATE TABLE filtro_promocion (
    id_usuario int NOT NULL,
    id_promocion int NOT NULL,
    _createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    _updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario),
    FOREIGN KEY (id_promocion) REFERENCES promocion(id_promocion)
);
CREATE TABLE filtro_empresa (
    id_usuario int NOT NULL,
    id_empresa int NOT NULL,
    _createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    _updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario),
    FOREIGN KEY (id_empresa) REFERENCES empresa(id_empresa)
);
CREATE TABLE comentario (
    id_comentario int NOT NULL AUTO_INCREMENT,
    detalle text,
    puntuacion int(2),
    id_usuario int NOT NULL,
    id_promocion_sucursal int NOT NULL,
    _createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    _updatedAt datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id_comentario),
    FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario),
    FOREIGN KEY (id_promocion_sucursal) REFERENCES promocion_sucursal(id_promocion_sucursal)
);
